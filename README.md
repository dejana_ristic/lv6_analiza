1.Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost znanstvenog kalkulatora, 
odnosno implementirati osnovne (+,-,*,/) i barem 5 naprednih (sin, cos, log, sqrt...) operacija. 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalktulator
{
    public partial class Form1 : Form
    {
        private double x,y,rez;
        public Form1()
        {
            InitializeComponent();
        }

        private void b_plus_Click(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(textBox1_result.Text);
                double y = double.Parse(textBox2_result.Text);
                lbRezultat.Text = "Rezultat: " + (x + y).ToString();


            }
            catch(Exception exception)
            {
                MessageBox.Show("Provjeri unos!");
            }
          
        }

        private void b_minus_Click(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(textBox1_result.Text);
                double y = double.Parse(textBox2_result.Text);
                lbRezultat.Text = "Rezultat: " + (x - y).ToString();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Provjeri unos!");
            }
        }

        private void b_puta_Click(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(textBox1_result.Text);
                double y = double.Parse(textBox2_result.Text);
                lbRezultat.Text = "Rezultat: " + (x * y).ToString();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Provjeri unos!");
            }
        }

        private void b_podijeljeno_Click(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(textBox1_result.Text);
                double y = double.Parse(textBox2_result.Text);
                lbRezultat.Text = "Rezultat: " + (x / y).ToString();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Provjeri unos!");
            }

        }

        private void b_pow_Click(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(textBox1_result.Text);
                rez = Math.Pow(x,2);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Provjeri unos!");
            }
        }

        private void b_sqrt_Click(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(textBox1_result.Text);
                rez = Math.Sqrt(x);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Provjeri unos!");
            }
        }

        private void b_log_Click(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(textBox1_result.Text);
                rez = Math.Log(x);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Provjeri unos!");
            }
        }

        private void b_sin_Click(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(textBox1_result.Text);
                rez = Math.Sin(x);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Provjeri unos!");
            }
        }

        private void b_cos_Click(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(textBox1_result.Text);
                rez = Math.Cos(x);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Provjeri unos!");
            }
        }

        private void b_tan_Click(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(textBox1_result.Text);
                rez = Math.Tan(x);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Provjeri unos!");
            }
        }

        private void b_jednako_Click(object sender, EventArgs e)
        {
            textBox1_result.Text = rez.ToString();
            textBox1_result.Show();
        }


        private void button_click(object sender, EventArgs e)
        {
            if (textBox1_result.Text == "0")
                textBox1_result.Clear();
            Button button = (Button)sender;
            textBox1_result.Text = textBox1_result.Text + button.Text;
        }

        private void b_obrisi_Click(object sender, EventArgs e)
        {
            textBox1_result.Text = "0";
        }
    }
}

2.Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, i u svakoj partiji se odabire nasumični pojam iz liste. 
Omogućiti svu funkcionalnost koju biste očekivali od takve igre. Nije nužno crtati vješala, 
dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo. 


using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        List<string> lista = new List<string>();
        private string pojam;
        private int found = 0, zivoti = 7, numFound=0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {

            using (System.IO.StreamReader reader = new System.IO.StreamReader("Igra.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    lista.Add(line);
                }
            }
            lBPojmovi.DataSource = null;
            lBPojmovi.DataSource = lista;
            pojam = lista[0];
            lbRezultat.Text = zivoti.ToString();
        }

   

        private void bTrazi_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < pojam.Length; i++)
            {
                if (pojam[i].ToString() == tbRijec.Text)
                {
                    lbPojam.Text += pojam[i];
                    found = 1;
                    numFound++;
                }
            }

            if (found != 1)
            {
                zivoti--;
                lbRezultat.Text = zivoti.ToString();
            }

            found = 0;

            if (zivoti == 0)
            {
                MessageBox.Show("Game over");
                Application.Exit();
            }
            if (numFound == pojam.Length)
            {
                MessageBox.Show("Bravoo!!");
                Application.Exit();
            }
        }

    }
}
